/*  Autores:  Daniel Alcal� Valera / Beatriz Crespo Torbado / Francisco Herreras G�mez
Pr�ctica: Practica  Analizador Sint�ctico Descendente Recursivo
*/
package es.unileon.procesadores.main;

import es.unileon.procesadores.parser.JFlexParserGenerator;
import es.unileon.procesadores.parser.Parser;

public class Initializer {

	/**
	 * Starts the execution: generates a 'Lexer.java' (analyzer) from 'Exercise.jflex' conditions and analizes the input provided in 'entrada.txt'.
	 * @param args
	 */
	public static void main(String[] args) {
		/* 	We first delete previous JFlex Parser and make the new one
			because could have been changes in our .jflex file */
		JFlexParserGenerator.execute();
		/*	Now we analize our .txt file with our analyzers up to date */
		Parser.execute("files/entrada.txt");
	}

}
