/*  Autores:  Daniel Alcal� Valera / Beatriz Crespo Torbado / Francisco Herreras G�mez
Pr�ctica: Practica  Analizador Sint�ctico Descendente Recursivo
*/

package es.unileon.procesadores.parser;

import java.io.*;

import es.unileon.procesadores.myjflex.Lexer;
import es.unileon.procesadores.token.Yytoken;

public class Parser {

	private static Lexer analizadorLexico = null;
	private Yytoken currentToken = null;

	public Parser(Lexer lex) {
		analizadorLexico = lex;
	}
	
	String currentType() {
		return currentToken.m_tktype;
	}

	private  void nextToken() {
		Yytoken prevToken = currentToken;
		try {
			currentToken = analizadorLexico.yylex();
			if (currentToken == null) {
				System.out.println("Fin de fichero");
				currentToken= new Yytoken(0, "", "EOF", prevToken.m_line, prevToken.m_column);
			}
		} catch (IOException e) {
			if (currentToken == null) {
				System.out.println("Fin de fichero" + e.toString());
			} else { 
				currentToken.Error("Error obteniendo el siguiente token despues de");
			}
		}
		prevToken=null;
	}
	
	// Inicio Producciones
	/*
	S --> Exp;
	Exp --> Term Exp'
	Exp' --> + Term Exp' | - Term Exp' | VACIO
	Term --> Factor Term'
	Term' --> * Factor Term' | / Factor Term' | VACIO
	Factor --> (Exp) | num
	 */
	public void S() {
		System.out.println("---> Comienza la sentencia <---");
		Exp();
		match("PTO_COMA");
		System.out.println("---> Finaliza la sentencia <---");
	}
	public void Exp() {
		Term();
		Exp_Prima();
	}
	
	public void Exp_Prima() {
		if (currentType().equals("OpSum")){
			match("OpSum");
			Term();
			Exp_Prima();
		} else {
			// Nunca deber�a haber una opci�n as�, pero bueno...
			System.out.print("");
		}
	}
	
	public void Term() {
		Factor();
		Term_Prima();
	}
	
	public void Term_Prima() {
		if (currentType().equals("OpMul")) {
			match("OpMul");
			Factor();
			Term_Prima();
		} else {
			// Nunca deber�a haber una opci�n as�, pero bueno...
			System.out.print("");
		}
	}
	

	public void Factor() {
		switch(currentType()) {
			case "ABREPAR" : 
				match("ABREPAR");
				Exp();
				match("CIERRAPAR");
				break;
			case "Num":
				match("Num");
				break;
			default: 
				currentToken.Error("Validando 'factor'");
				break;
		}
	}
	
	// Fin Producciones
	// Metodo match para comparar tokens
	public void match(String refStrToken) {
		if (currentType().equals(refStrToken)) {
			System.out.println("[OK] Token correcto: "+currentType());
			nextToken();
		} else {
			currentToken.ErrorMatch(refStrToken);
		}
	}

	
	public static void execute(String file) {
		if (file == null) {
			System.out.println("Debe indicar un fichero para analizar.");
		} else if (file.length() == 0) {
			System.out.println("Ha indicado un nombre vac�o para el fichero.");
		} else {
			try {
				java.io.FileInputStream stream = new java.io.FileInputStream(file);
			    java.io.Reader reader = new java.io.InputStreamReader(stream);
				Lexer lex= new Lexer(reader);
				Parser asd=new Parser(lex);
				asd.nextToken();
				do { 
					asd.S();
				} while (!asd.currentType().equals("EOF"));
			} catch(IOException x) {
				System.out.println("Error leyendo "+x.toString()+(analizadorLexico.yytext()));
			}
		}
	}

}
