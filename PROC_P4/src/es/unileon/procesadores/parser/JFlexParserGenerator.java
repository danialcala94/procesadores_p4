/*  Autores:  Daniel Alcal� Valera / Beatriz Crespo Torbado / Francisco Herreras G�mez
Pr�ctica: Practica  Analizador Sint�ctico Descendente Recursivo
*/

package es.unileon.procesadores.parser;

import java.io.File;

import es.unileon.procesadores.myjflex.Lexer;

public class JFlexParserGenerator {
	private static final String OUTPUT_DIR = "src/es/unileon/procesadores/myjflex/";
	private static final String FILES_DIR = "files/";
	private static final String EXERCISE_ONE = FILES_DIR + "Exercise.jflex";
	private static final String EXAMPLE_ONE = FILES_DIR + "entrada.txt";
	
	/**
	 * Generates the analyzer ('Lexer.java').
	 */
	public static void execute() {
		String paths[] = {EXERCISE_ONE};
		generateAllAnalyzers(paths, OUTPUT_DIR);
	}
	
	/**
	 * Deletes the previous analyzers if existing.
	 * @return
	 */
	private static boolean deletePreviousAnalyzers() {
		int cont = 0;
		
		if (new File(OUTPUT_DIR + "Lexer.java").delete()) {
			cont++;
		}

		System.out.println("Deleted " + cont + " analyzers.");
		
		if (cont == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * It creates an analyzer using JFlex.
	 * @param path
	 * @param outputDir
	 */
	private static void generate(String path, String outputDir) {
		String args[] = {path, "-d", outputDir};
		jflex.Main.main(args);
	}
	
	/**
	 * Generates the analizer(s)
	 * @param path
	 * @param outputDir
	 */
	private static void generateAllAnalyzers(String[]path, String outputDir) {
		deletePreviousAnalyzers();
		
		for (int i = 0; i < path.length; i++) {
			System.out.println("Generating analyzer number " + (i + 1));
			generate(path[i], outputDir);
		}
	}
}
