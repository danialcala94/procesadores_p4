// Authors: ALCAL� VALERA, DANIEL; CRESPO TORBADO, BEATRIZ; HERRERAS G�MEZ, FRANCISCO

package es.unileon.procesadores.myjflex;

//Area de codigo, importaciones y paquetes

import java.io.*;
import es.unileon.procesadores.token.Yytoken;
%%
//Area de opciones y declaraciones
%class Lexer
%line
%column
%public


Digito = [0-9]
Numero = {Digito} {Digito}*
Finlinea = \n|\r|\r\n
Blanco = {Finlinea}|[ \t\f]

%%
//Area de reglas y acciones

{Blanco} { System.out.print(""); }
";" { System.out.println("LEX:PTOCOMA"); return new Yytoken(1, yytext(), "PTO_COMA", yyline, yycolumn); }
"(" { System.out.println("LEX:ABREPAR"); return new Yytoken(1, yytext(), "ABREPAR", yyline, yycolumn); }
")" { System.out.println("LEX:CIERRAPAR"); return new Yytoken(1, yytext(), "CIERRAPAR", yyline, yycolumn); }
{Numero} { System.out.println("LEX:NUM:" + yytext()); return new Yytoken(1, yytext(), "Num", yyline, yycolumn); }
[\+\-] { System.out.println("LEX:SUM:" + yytext()); return new Yytoken(1, yytext(), "OpSum", yyline, yycolumn); }
"*" { System.out.println("LEX:MUL:" + yytext()); return new Yytoken(1, yytext(), "OpMul", yyline, yycolumn); }
"/" { System.out.println("LEX:MUL:" + yytext()); return new Yytoken(1, yytext(), "OpMul", yyline, yycolumn); }
. {System.out.println("LEX:Error: encontrado " + yytext() + " en linea " + (yyline+1) + " columna " + (yycolumn+1)); }




 

